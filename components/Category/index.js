/*
  This example requires some changes to your config:
  
  ```
  // tailwind.config.js
  module.exports = {
    // ...
    plugins: [
      // ...
      require('@tailwindcss/aspect-ratio'),
    ],
  }
  ```
*/
const products = [
  {
    id: 1,
    name: "Бизнес эрхлэхэд бэлэн үү?",
    href: "#",
    price:
      "  Туршлагатай бизнес эрхлэгчид танд бизнесээ хэрхэн босгосон үнэт туршлагаасаа танилцуулах ба та өөрт тохирсон бизнесийг сонгож шууд эхлүүлэх боломжтой. ",
    imageSrc:
      "https://tailwindui.com/img/ecommerce-images/category-page-04-image-card-01.jpg",
    imageAlt:
      "Tall slender porcelain bottle with natural clay textured body and cork stopper.",
  },
  {
    id: 2,
    name: "Онлайн сургалт",
    href: "#",
    price:
      "Өөрийн хүсэл, сонирхолд нийцүүлэн онлайн сургалтанд сууж, шалгалт өгч, сертификат авах боломжтой.",
    imageSrc:
      "https://tailwindui.com/img/ecommerce-images/category-page-04-image-card-02.jpg",
    imageAlt:
      "Olive drab green insulated bottle with flared screw lid and flat top.",
  },
  {
    id: 3,
    name: "зөвлөгөө",
    href: "#",
    price:
      "              Та онлайнаар бүх төрлийн зөвлөгөөнүүдийг мэргэжлийн зөвлөхүүдтэй шууд холбогдон авах боломжтой",
    imageSrc:
      "https://tailwindui.com/img/ecommerce-images/category-page-04-image-card-03.jpg",
    imageAlt:
      "Person using a pen to cross a task off a productivity paper card.",
  },

  // More products...
];

export default function Example() {
  return (
    <div className="bg-white">
      <div className="mx-auto  py-8 px-2 sm:py-12 sm:px-4 lg:max-w-8xl lg:px-4">
        <h2 className="sr-only">Products</h2>

        <div className="grid grid-cols-1 gap-y-10 gap-x-6 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-3 xl:gap-x-8">
          {products.map((product) => (
            <a key={product.id} href={product.href} className="group">
              <div className=" w-full overflow-hidden rounded-lg bg-gray-200 ">
                <img
                  src={product.imageSrc}
                  alt={product.imageAlt}
                  className="h-full w-full object-cover object-center "
                />
              </div>
              <h3 className="mt-4 text-2xl text-gray-700">{product.name}</h3>
              <p className="mt-1 text-sm font-medium text-gray-900">
                {product.price}
              </p>
            </a>
          ))}
        </div>
      </div>
    </div>
  );
}
