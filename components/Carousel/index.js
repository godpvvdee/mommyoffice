import React from "react";
import Slider from "react-slick";
import Link from "next/link";
const BannerSlider = () => {
  const settings = {
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 5000,
    pauseOnHover: true,
  };

  const banners = [
    {
      image:
        "https://kajabi-storefronts-production.kajabi-cdn.com/kajabi-storefronts-production/themes/1625426/settings_images/AfUW4OZXRL2Z8evkmWos_Banner.png",
      description:
        "Get a better understanding of where your traffic is coming from.",
      href: "#",
    },
    {
      image:
        "https://kajabi-storefronts-production.kajabi-cdn.com/kajabi-storefronts-production/themes/1625426/settings_images/eWzKtriJRiqeIYoTW9Pk_courses.png",
      description: "Speak directly to your customers in a more meaningful way.",
      href: "#",
    },
  ];
  return (
    <div>
      <Slider {...settings}>
        {banners?.map((b, index) => (
          <div
            key={index}
            className="w-full  flex items-center justify-center "
          >
            {/* <div className="mt-5 sm:mt-8  absolute sm:flex sm:justify-center lg:justify-start">
              <div className="rounded-md shadow">
                <a
                  href="#"
                  className=" rounded-md border border-transparent bg-gray-600 px-8 py-3 text-base font-medium text-white hover:bg-indigo-700 md:py-4 md:px-10 md:text-lg"
                >
                  Онлайн сургалт
                </a>
              </div>
            </div> */}
            <a>
              <img
                height={500}
                style={{ objectFit: "cover", width: "100%" }}
                src={b.image}
                alt="ymar neg ym"
              />
            </a>
            {b.link}
          </div>
        ))}
      </Slider>
    </div>
  );
};

export default BannerSlider;
