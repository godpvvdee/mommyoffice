import {
  BoltIcon,
  ChatBubbleBottomCenterTextIcon,
  GlobeAltIcon,
  ScaleIcon,
} from "@heroicons/react/24/outline";

export default function Banner() {
  return (
    <div className="bg-black py-12">
      <div className="mx-auto max-w-7xl px-4 sm:px-6 lg:px-8">
        <div className="lg:text-center">
          <p className=" text-3xl font-bold leading-8 tracking-tight text-yellow-500 sm:text-4xl">
            Онлайн сургалт, зөвлөгөөний хамгийн өргөн сонголт
          </p>
          <p className="mt-4 max-w-2xl text-xl text-yellow-500 lg:mx-auto">
            Боломж Үргэлж Таны Дэргэд Байдаг
          </p>
        </div>
      </div>
    </div>
  );
}
