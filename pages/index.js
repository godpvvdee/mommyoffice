import Header from "../components/Header";
import Carousel from "../components/Carousel";
import Category from "../components/Category";
import Banner from "../components/Banner";
export default function Home() {
  return (
    <div>
      <Header></Header>
      <div
        data-aos="fade-zoom-in"
        data-aos-offset="200"
        data-aos-easing="ease-in-sine"
        data-aos-duration="600"
      >
        <Carousel></Carousel>
      </div>
      <Banner></Banner>
      <Category></Category>
    </div>
  );
}
